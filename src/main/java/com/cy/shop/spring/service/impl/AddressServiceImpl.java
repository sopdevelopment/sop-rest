package com.cy.shop.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.shop.spring.dao.AddressDao;
import com.cy.shop.spring.entity.Address;
import com.cy.shop.spring.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService{

	@Autowired
	private AddressDao addressDao;
	
	@Override
	public List<Address> getAddresses() {
		return addressDao.getAddresses();
	}

	public AddressDao getAddressDao() {
		return addressDao;
	}

	public void setAddressDao(AddressDao addressDao) {
		this.addressDao = addressDao;
	}

}
