package com.cy.shop.spring.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="shop")
public class Shop implements Serializable{

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 254002121680554629L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="shop_id")
	private long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;
	
	@Column(name="url")
	private String url;
	
	@OneToMany(mappedBy="shop")
	private Set<Address> addresses = new HashSet<Address>();

	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="user_shop",
	           joinColumns=@JoinColumn(name="shop_id", referencedColumnName="shop_id"),
	           inverseJoinColumns=@JoinColumn(name="user_id", referencedColumnName="user_id"))
	private Set<User> users = new HashSet<User>(); 
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
