package com.cy.shop.spring.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cy.shop.spring.dao.AddressDao;
import com.cy.shop.spring.entity.Address;

@Transactional
@Repository
public class AddressDaoImpl implements AddressDao{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Address> getAddresses() {
		TypedQuery<Address> query = entityManager.createQuery("from Address", Address.class);
		return query.getResultList();
	}

	@Override
	public List<Address> getAddressesByShopId(long shopId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address getAddressById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteAddressById(long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Address createAddress(Address address) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address updateAddress(Address address) {
		// TODO Auto-generated method stub
		return null;
	}

}
