package com.cy.shop.spring.dao;

import java.util.List;

import com.cy.shop.spring.entity.Address;

public interface AddressDao {

	public List<Address> getAddresses();
	
	public List<Address> getAddressesByShopId(long shopId);
	
	public Address getAddressById(long id);
	
	public boolean deleteAddressById(long id);
	
	public Address createAddress(Address address);
	
	public Address updateAddress(Address address);
}
