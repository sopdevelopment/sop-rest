package com.cy.shop.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cy.shop.spring.entity.Address;
import com.cy.shop.spring.service.AddressService;

@Controller
@RequestMapping("address")
public class AddressController {

	@Autowired
	private AddressService addressService;
	
	@GetMapping("addresses")
	public ResponseEntity<List<Address>> getAddresses() {
		System.out.println("----------------Service------------------");
		List<Address> addresses = addressService.getAddresses();
		return new ResponseEntity<List<Address>>(addresses, HttpStatus.OK);
	}
}
