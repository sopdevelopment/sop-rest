CREATE DATABASE  IF NOT EXISTS `shop_demo` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `shop_demo`;

DROP TABLE IF EXISTS `shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop` (
  `shop_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(145) NOT NULL,
  `description` varchar(145) NOT NULL,
  `url` varchar(145) NOT NULL,
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `shop` VALUES (1,'Amazon', 'Amazon', 'http://www.amazon.de');
INSERT INTO `shop` VALUES (2,'Ebay', 'Ebay', 'http://www.ebay.de');
INSERT INTO `shop` VALUES (3,'Bike24', 'Bike24', 'http://www.bike24.de');
INSERT INTO `shop` VALUES (4,'Bike Discount', 'Bike Discount', 'http://www.bike-discount.de');
INSERT INTO `shop` VALUES (5,'Fahrrad', 'Fahrrad', 'http://www.fahrrad.de');
INSERT INTO `shop` VALUES (6,'Bike Components', 'Bike Components', 'http://www.bike-components.de');
INSERT INTO `shop` VALUES (7,'Bruegelmann', 'Bruegelmann', 'http://www.bruegelmann.de');

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `address_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `zip` varchar(30) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `street` varchar(145) NOT NULL,
  `number` varchar(10) NOT NULL,
  `primarily` tinyint(1),
  `shop_id` bigint(20),
  PRIMARY KEY (`address_id`),
  FOREIGN KEY(shop_id) REFERENCES shop(shop_id)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `address`(address_id, zip, city, country, street, number, primarily, shop_id) VALUES (1,'90439', 'Nürnberg', 'Deutschland', 'Kurt-Karl-Doberer-Strasse', '72', false, 1);
INSERT INTO `address`(address_id, zip, city, country, street, number, primarily, shop_id) VALUES (2,'91058', 'Erlangen', 'Deutschland', 'Max-Strasse', '12', TRUE, 1);

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `birthday` datetime NOT NULL,
  `email` varchar(145) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Stephan','Kraus', '1976-10-24 10:00:00','sk@ancud.de', 'testtest');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `user_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_shop` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shop_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY(shop_id) REFERENCES shop(shop_id),
  FOREIGN KEY(user_id) REFERENCES user(user_id)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `user_shop`(user_id, shop_id) VALUES (1, 2);
